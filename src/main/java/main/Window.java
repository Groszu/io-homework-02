package main;

import com.l2fprod.common.swing.JTaskPane;
import com.l2fprod.common.swing.JTipOfTheDay;
import com.l2fprod.common.swing.tips.DefaultTip;
import com.l2fprod.common.swing.tips.DefaultTipModel;
import org.apache.log4j.*;
import org.freixas.jcalendar.JCalendarCombo;
import org.jfree.chart.JFreeChart;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.SimpleDateFormat;


/*

    @Author: Radoslaw Grosz

 */

public class Window extends JFrame{

    JMenu file, edit, view, calc, help;
    JMenuBar menuBar;
    JMenuItem author, save;
    JDialog DAuthor;
    JCalendarCombo calendar = new JCalendarCombo(JCalendarCombo.DISPLAY_DATE, true);
    JTaskPane nav = new JTaskPane();
    SimpleDateFormat dateFormat;
    JFreeChart vChart;

    public Window() {

        super("Radosław Grosz U-15583");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(800, 600);
        this.setLocationRelativeTo(null);  // Center window
        this.setResizable(false);
        this.setLayout(new FlowLayout());
        this.setBackground(Color.lightGray);


        //Menu
        menuBar = new JMenuBar();
        this.setJMenuBar(menuBar);

        file = new JMenu("Plik");
        edit = new JMenu("Edycja");
        view = new JMenu("Widok");
        calc = new JMenu("Obliczenia");
        help = new JMenu("Pomoc");
        author = new JMenuItem("O autorze");
        save = new JMenuItem("Zapisz");
        save.addActionListener(new SaveListener());
        DAuthor = new JDialog(this, "O autorze");

        menuBar.add(file);
        menuBar.add(edit);
        menuBar.add(view);
        menuBar.add(calc);
        menuBar.add(help);
        help.add(author);
        file.add(save);

        DefaultTipModel model = new DefaultTipModel();
        model.add(new DefaultTip("tip1","Treść pierwszej porady."));
        model.add(new DefaultTip("tip2","Treść drugiej porady."));
        JTipOfTheDay tips = new JTipOfTheDay(model);
        tips.showDialog(this);

        Logger logger = Logger.getLogger(Window.class);
        Logger root = Logger.getRootLogger();
        Appender console = new ConsoleAppender();
        root.addAppender(console);
        BasicConfigurator.configure();
        logger.info("This is my first log4j's statement");

        String log4jConfigFile = System.getProperty("user.dir")
                + File.separator + "log4j.properties";
        PropertyConfigurator.configure(log4jConfigFile);
        logger.debug("this is a debug log message");
        logger.info("this is a information log message");
        logger.warn("this is a warning log message");


        this.add(new InputsPanel());
        this.add(new MainPanel());
        this.add(new CalcPanel());
        this.add(new ResultPanel());
        this.add(new StatusPanel());


        author.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JLabel l = new JLabel("Autor: Radoslaw Grosz");
                DAuthor.add(l);
                DAuthor.setSize(200, 200);
                DAuthor.setLocationRelativeTo(null);
                DAuthor.setVisible(true);
            }
        });

        this.setVisible(true);
    }
}
