package main;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


public class SaveListener implements ActionListener {

    private static JFileChooser fileChooser = new JFileChooser();

    public void actionPerformed(ActionEvent e) {
        int userSelection = fileChooser.showSaveDialog(App.getFrame());

        if (userSelection == JFileChooser.APPROVE_OPTION) {
            File fileToSave = fileChooser.getSelectedFile();
            try {
                fileToSave.createNewFile();
            } catch (IOException ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(App.getFrame(), "Coś poszło nie tak");
            }
            try {
                FileWriter myWriter = new FileWriter(fileToSave.getAbsolutePath());
                StringBuilder data = new StringBuilder();
                for (int i = 0; i < 5; i++) {
                    for (int j = 0; j < 5; j++) {
                        data.append(MainPanel.getTable().getValueAt(i, j));
                        data.append(" ");
                    }
                    data.append("\n");
                }
                myWriter.write(String.valueOf(data));
                myWriter.close();
                StatusPanel.getInfoArea().setText("Pomyślnie zapisano");
            } catch (IOException err) {
                System.out.println("An error occurred.");
                err.printStackTrace();
                JOptionPane.showMessageDialog(App.getFrame(), "Coś poszło nie tak");
            }
        }
    }
}
