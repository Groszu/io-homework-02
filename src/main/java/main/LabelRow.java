package main;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class LabelRow extends JLabel {

    public LabelRow(){
        super();
        this.setText("wybierz wiersz: ");
        this.setPreferredSize(new Dimension(220, 20));
        this.setBorder(new EmptyBorder(0, 70, 0, 0));
    }
}
