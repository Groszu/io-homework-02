package main;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CalcListener implements ActionListener {

    private static Table table = MainPanel.getTable();

    public void actionPerformed(ActionEvent e) {
        // Calculate sum of all numbers in table
        if (CalcPanel.getOperationsList().getSelectedValue() == "Suma elementów") {
            ScrollPane.appendText("Suma elementów jest równa: " + getSum(table) + "\n");
        }

        // Get average value of all numbers in table
        if (CalcPanel.getOperationsList().getSelectedValue() == "Średnia elementów") {
            double avg = (double) getSum(table) / (table.getRowCount() * table.getColumnCount());
            ScrollPane.appendText("Średnia elementów jest równa: " + avg + "\n");
        }

        // Get min and max value in table
        if (CalcPanel.getOperationsList().getSelectedValue() == "Wartość max i min") {
            ScrollPane.appendText("Min: " + getMin(table) + ", max: " + getMax(table) + "\n");
        }
    }

    private double getSum(JTable table) {
        double amount = 0;
        for (int i = 0; i < table.getRowCount(); i++) {
            for (int j = 0; j < table.getColumnCount(); j++) {
                amount = amount + Double.parseDouble(table.getValueAt(i, j) + "");
            }
        }
        return amount;
    }

    private double getMax(JTable table) {
        double max = new Double(table.getValueAt(0, 0).toString());
        for (int i = 0; i < table.getRowCount(); i++) {
            for (int j = 0; j < table.getColumnCount(); j++) {
                if (max < new Double(table.getValueAt(i, j).toString())) {
                    max = new Double(table.getValueAt(i, j).toString());
                }
            }
        }
        return max;
    }

    private double getMin(JTable table) {
        double min = new Double(table.getValueAt(0, 0).toString());
        for (int i = 0; i < table.getRowCount(); i++) {
            for (int j = 0; j < table.getColumnCount(); j++) {
                if (min > new Double(table.getValueAt(i, j).toString())) {
                    min = new Double(table.getValueAt(i, j).toString());
                }
            }
        }
        return min;
    }
}
