package main;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;

public class Slider extends JSlider {

    private static int row, column;

    public Slider(){
        super(0,4,0);
        this.setPaintTrack(true);
        this.setPaintTicks(true);
        this.setPaintLabels(true);
        this.setPreferredSize(new Dimension(150, 50));
        this.setMajorTickSpacing(1);
        this.setMinorTickSpacing(1);

        this.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                row = InputsPanel.getRowSlider().getValue();
                column = InputsPanel.getColumnSlider().getValue();
            }
        });
    }

    public static int getRow(){
        return row;
    }

    public static int getColumn(){
        return column;
    }
}
