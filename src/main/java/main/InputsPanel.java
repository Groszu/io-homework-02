package main;

import javax.swing.*;
import java.awt.*;

public class InputsPanel extends JPanel {

    private static Slider rowSlider, columnSlider;
    private static TextFieldNumber number;

    public InputsPanel(){
        super();
        this.setPreferredSize(new Dimension(700, 80));
        this.setLayout(new FlowLayout(1));
        this.add(new LabelNumber());
        this.add(new LabelRow());
        this.add(new LabelColumn());
        this.add(number = new TextFieldNumber());
        this.add(rowSlider = new Slider());
        this.add(columnSlider = new Slider());
    }

    public static Slider getRowSlider(){
        return rowSlider;
    }

    public static Slider getColumnSlider(){
        return columnSlider;
    }

    public static TextFieldNumber getNumber(){
        return number;
    }
}
