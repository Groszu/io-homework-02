package main;

import org.freixas.jcalendar.DateEvent;
import org.freixas.jcalendar.DateListener;
import org.freixas.jcalendar.JCalendarCombo;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Calendar extends JCalendarCombo {

    private static Date date;
    private static SimpleDateFormat format;

    public Calendar(){
        super();
        format = new SimpleDateFormat("yyyy/MM/dd");
        this.setDateFormat(format);
        this.addDateListener(new DateListener() {
            public void dateChanged(DateEvent dateEvent) {
                String date = format.format(MainPanel.getCalendar().getDate()).toString() + "\n";
                ScrollPane.appendText(date);
            }
        });
    }
}
