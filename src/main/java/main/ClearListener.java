package main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ClearListener implements ActionListener {

    public void actionPerformed(ActionEvent e) {
        MainPanel.getTable().resetTable();
    }
}
