package main;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class MainPanel extends JPanel {

    private static Table table;
    private static Calendar calendar;

    public MainPanel(){
        super();
        this.setPreferredSize(new Dimension(700, 130));
        this.setLayout(new FlowLayout(0));
        this.setBorder(new EmptyBorder(10, 10, 0, 10));
        this.add(table = new Table());
        this.init();
    }

    public void init(){
        JButton bClear = new JButton("Zerowanie");
        JButton bSet = new JButton("Dodaj");
        JButton bSave = new JButton("Zapisz");

        bClear.addActionListener(new ClearListener());
        bSet.addActionListener(new SetListener());
        bSave.addActionListener(new SaveListener());

        this.add(bSet);
        this.add(bClear);
        this.add(bSave);
        this.add(calendar = new Calendar());

    }

    public static Table getTable(){
        return table;
    }
    public static Calendar getCalendar(){return calendar;}
}
                                                                                                                                                                                                                                          