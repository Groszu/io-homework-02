package main;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class TextFieldNumber extends JTextField {

    public TextFieldNumber(){
        super();
        this.setPreferredSize((new Dimension(150, 30)));
        this.setBorder(new EmptyBorder(0, 0, 0, 50));
    }
}
