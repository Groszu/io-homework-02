package main;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;

public class ResultPanel extends JPanel {

    private static TitledBorder border;
    private static ScrollPane scrollPane;

    public ResultPanel(){
        super();
        border = BorderFactory.createTitledBorder("Wyniki");
        border.setTitleJustification(TitledBorder.CENTER);
        this.setPreferredSize(new Dimension(700, 190));
        this.setBorder(border);
        this.add(scrollPane = new ScrollPane());
    }

    public static ScrollPane getScrollPane(){
        return scrollPane;
    }
}
