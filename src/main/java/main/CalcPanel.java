package main;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class CalcPanel extends JPanel {

    private static JList<String> operationsList;
    private static String[] operations = new String[]{"Suma elementów", "Średnia elementów", "Wartość max i min"}; // List of available operations

    public CalcPanel(){
        super();
        this.setPreferredSize(new Dimension(600, 80));
        this.setLayout(new FlowLayout(0));
        this.add(new LabelCalc());
        this.init();
    }

    private void init() {
        JButton bCalc = new JButton("Oblicz");
        operationsList = new JList<>(operations);
        operationsList.setBorder(new EmptyBorder(0, 10, 0, 15));
        bCalc.addActionListener(new CalcListener());
        this.add(operationsList);
        this.add(bCalc);
        this.add(new ToolBar());
    }

    public static JList<String> getOperationsList(){
        return operationsList;
    }
}
