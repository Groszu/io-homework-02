package main;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SetListener implements ActionListener {

    public void actionPerformed(ActionEvent e) {
        try {
            double number = filterNumber();
            MainPanel.getTable().setValue(number, Slider.getRow(), Slider.getColumn());
        } catch (Exception err) {
            JOptionPane.showMessageDialog(App.getFrame(), "Podaj prawidłową wartość!");
        }
    }

    private double filterNumber(){
        if(InputsPanel.getNumber().getText().indexOf(',') != -1) {
            String number = InputsPanel.getNumber().getText();
            int index = InputsPanel.getNumber().getText().indexOf(',');
            char[] numberChars = number.toCharArray();
            numberChars[index] = '.';
            number = String.valueOf(numberChars);
            return Double.parseDouble(number);
        }else return Double.parseDouble(InputsPanel.getNumber().getText());
    }
}

