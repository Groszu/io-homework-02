package main;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class LabelNumber extends JLabel {

    public LabelNumber(){
        super();
        this.setText("Podaj liczbe: ");
        this.setPreferredSize(new Dimension(170, 20));
        this.setBorder(new EmptyBorder(0, 80, 0, 0));
    }
}
