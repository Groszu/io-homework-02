package main;

import java.awt.*;

public class App {

    private static Window frame;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Window();
            }
        });
    }

    public static Window getFrame(){
        return frame;
    }
}