package main;

import javax.swing.*;
import java.awt.*;

public class ToolBar extends JToolBar {

    public ToolBar(){
        super();
        this.setFloatable(false);
        this.init();
    }

    private void init() {
        Icon setIcon = this.scaleIcon("img/import.png");
        Icon clearIcon = this.scaleIcon("img/clear.png"); //import and scale icons. Used method below.
        Icon saveIcon = this.scaleIcon("img/save.png");
        JButton bSet = new JButton(setIcon);
        JButton bClear = new JButton(clearIcon);
        JButton bSave = new JButton(saveIcon);

        bSet.addActionListener(new SetListener());
        bClear.addActionListener(new ClearListener());
        bSave.addActionListener(new SaveListener());

        this.add(bSet);
        this.add(bClear);
        this.add(bSave);
    }

    // Import icon from relative path and scale
    private ImageIcon scaleIcon(String path) {
        ImageIcon imageIcon = new ImageIcon(path);
        Image image = imageIcon.getImage();
        Image newImg = image.getScaledInstance(40, 40, Image.SCALE_SMOOTH);
        return new ImageIcon(newImg);
    }
}
