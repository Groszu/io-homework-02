package main;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class StatusPanel extends JPanel {

    private static TextAreaInfo info;

    public StatusPanel(){
        super();
        this.setLayout(new FlowLayout());
        this.setPreferredSize(new Dimension(800, 35));
        this.setBorder(new EmptyBorder(10, 0, 0, 0));
        this.add(new LabelInfo());
        this.add(info = new TextAreaInfo());
        this.add(new LabelStatus());
        this.add(new TextAreaStatus());
    }

    public static TextAreaInfo getInfoArea(){
        return info;
    }
}
