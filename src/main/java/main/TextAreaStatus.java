package main;

import javax.swing.*;

public class TextAreaStatus extends JTextArea {

    public TextAreaStatus(){
        super(1,10);
        this.setText("ON");
        this.setEditable(false);
    }
}
