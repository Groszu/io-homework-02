package main;

import javax.swing.*;

public class Table extends JTable {

    public Table(){
        super(5,5);
        this.setDefaultEditor(Object.class, null);
        resetTable();
    }

    public void resetTable() {
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                this.setValueAt(0, i, j);
            }
        }
    }

    public void setValue(double value, int row, int column){
        this.setValueAt(value, row, column);
    }
}
