package main;

import javax.swing.*;
import java.awt.*;

public class ScrollPane extends JScrollPane {

    private static TextAreaResult result;

    public ScrollPane(){
        super(result = new TextAreaResult());
        this.setPreferredSize(new Dimension(680, 160));
    }

    public static void appendText(String text){
        result.append(text);
    }
}
